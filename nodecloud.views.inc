<?php

/**
 *  Implementation of hook_views_style_plugins()
 */
function nodecloud_views_style_plugins() {
  $plugins = array();
  $plugins['nodecloud'] = array(
    'name' => t('Node Cloud'),
    'theme' => 'nodecloud_display',
    'summary_theme' => 'nodecloud_display',
    'needs_fields' => true,
  );
  return $plugins;
}

/**
 *  Implementation of the display theme
 */
function theme_nodecloud_display(&$view, &$items, $type) {
  // detect if the second sort field is something that we'll get back
  if (isset($view->sort[1])) {
    $sort_field = strtr($view->sort[1]['field'], '.', '_');
    $is = (array) $items[0];
    if (array_key_exists($sort_field, $is)) {
      $size_by = $sort_field;
      $order_by = $view->sort[1]['sortorder'];
    }
  }
  
  // if we couldn't set $size_by, see if there is a numeric result in the first item's fields
  if (!isset($size_by)) {
    foreach($items[0] as $key => $field) {
      if (is_numeric($field)) {
        $size_by = $key;
        $order_by = 'DESC';
        break;
      }
    }
  }
  
  if (!isset($size_by)) {
    $size_by = 'nid'; // guaranteed to be in every view
    $order_by = 'DESC';
  }
  
  $fields = _views_get_fields();
  
  $total = count($items);
  $half_way = ceil($total/2);

  // find the min and max values of the $size_by field
  foreach($items as $key => $item) {
    $sizes[] = $item->$size_by;
  }
  
  if ($order_by == 'DESC') {
    $realmax = max($sizes);
    $realmin = min($sizes);
  } else {
    $realmax = min($sizes);
    $realmin = max($sizes);
  }

  if (!isset($realmax)) {
    $realmax = 0;
  }
  
  if (!isset($realmin)) {
    $realmin = 0;
  }
  
  if (($realmax - $realmin) == 0) {
    $realmax += 1; // fudge factor incase things go badly....
  }
  
  // resize max and min on to the range of text sizes we've defined
  $localmax = variable_get('nodecloud_max', NODECLOUD_MAX);
  $localmin = variable_get('nodecloud_min', NODECLOUD_MIN);
  
  $strengths = array();
  
  foreach($items as $key => $item) {
    $strength = ((($localmax - $localmin) * (($item->$size_by - $realmin) / ($realmax - $realmin))) / $localmin) + $localmin ;
    $strengths[$key]['item'] = $item;
    $strengths[$key]['em'] = $strength;
  }
  
  $clouds = '';
  foreach ($strengths as $unit) {
    $clouds .= theme('nodecloud_item', $view, $unit['item'], $unit['em']);
  }
  
  if ($clouds) {
    drupal_add_css(drupal_get_path('module', 'nodecloud') . '/nodecloud.css');
    return "<div style=\"line-height: ". ($localmax) . "em;\" class=\"nodecloud-cloud\">" . $clouds . "</div>";
  }
}

/**
 *  Formats individual cloud items.
 */
function theme_nodecloud_item($view, $item, $size) {
  foreach ($view->field as $field) {
    if ($fields[$field['id']]['visible'] !== FALSE) {
      if ($field['label']) {
        $cloud .= "<span class=\"view-label ". views_css_safe('view-label-'. $field['queryname']) ."\">" . $field['label'] . "</span>\n";
      }
      $cloud .= "<span class=\"view-field ". views_css_safe('view-data-'. $field['queryname']) ."\">" . views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $item, $view) . "</span>\n";
    }
  }
  if (!trim($cloud)) {
    $node = node_load($item->nid);
    $cloud = l($node->title, 'node/' . $node->nid);
  }
  return "<span style=\"font-size: ". $size ."em;\" class=\"view-item ". views_css_safe('view-item-'. $view->name) ."\">". $cloud ."</span>\n"; 
}